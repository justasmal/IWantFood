import express from "express";
import webpack from "webpack";
import webpackMiddleware from "webpack-dev-middleware";
import webpackHotMiddleware from "webpack-hot-middleware";
import config, { output } from "./../webpack.config.js";
import bodyParser from "body-parser";
import { join } from "path";
const passport = require("passport");
import session from "express-session";
const mongoose = require("mongoose");
const LocalStrategy = require("passport-local").Strategy;
require("dotenv").config();


const isDeveloping = process.env.ENV_TYPE !== "production";
const port = isDeveloping ? 3000 : process.env.WEB_PORT;
let app = express();

if (isDeveloping) {
    const compiler = webpack(config);
    const middleware = webpackMiddleware(compiler, {
        publicPath: output.publicPath,
        contentBase: join("frontend", "src"),
        stats: {
            colors: true,
            hash: false,
            timings: true,
            chunks: false,
            chunkModules: false,
            modules: false
        }
    });

    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));
    app.get("/", function response(req, res) {
        res.write(middleware.fileSystem.readFileSync(join(__dirname, "..", "frontend", "dist", "index.html")));
        res.end();
    });
} else {
    app.use(join(__dirname, "./frontend/dist"));
    app.get("/", function response(req, res) {
        res.sendFile(join(__dirname, "frontend", "dist", "index.html"));
    });
}
const mongoURL = process.env.MONGODB_SERVER;
mongoose.Promise = global.Promise;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
    secret: process.env.SESSION_SECRET
}));
app.use(passport.initialize());
app.use(passport.session());
mongoose.connect(mongoURL);
const User = require("./models/user.models");
passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.listen(port, error => {
    if (error) {
        
        return console.error(error);
    }

    console.info("==> 🌎 Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port);
});
export default app;