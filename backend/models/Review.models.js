const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var Review = new Schema({
    email: String,
    placeId: String,
    placeFavorit: Boolean
}, { timestamps: true });


module.exports = mongoose.model("Review", Review);