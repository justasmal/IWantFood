const passportLocalMongoose = require("passport-local-mongoose");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
import bcrypt from "bcrypt";


var User = new Schema({
    password: String,
    email: { type: String, required: true, unique: true, lowercase: true },
    googleid: String,
    googletoken: String,
    name: String,
    accessToken: { type: String }
}, { timestamps: true });


User.pre("save", function(next) {
    var user = this;
    if(!user.isModified("password")) return next();
    bcrypt.genSalt(10, function(err, salt) {
        if(err) return next(err);
        bcrypt.hash(user.password, salt, function(err, hash) {
            if(err) return next(err);
            user.password = hash;
            next();
        });
    });
});

User.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if(err) return cb(err);
        cb(null, isMatch);
    });
};

User.plugin(passportLocalMongoose);

module.exports = mongoose.model("User", User);