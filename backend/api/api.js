import app from "./../index";
import User from "../models/user.models";
import Review from "../models/Review.models";
import Validation from "../validations/user.schema";
import ReviewValidation from "../validations/Review.schema";
import CoordValidation from "../validations/coordinates.schema";
const passport = require("./auth");
import getJSON from "get-json";

/** GOOGLE AUTH */

app.get("/api/auth/google", passport.authenticate("google",
    {scope: ["profile", "email"]})
);

app.get("/api/auth/google/callback", 
    passport.authenticate("google", { failureRedirect: "/api/auth/notvalid" }),
    function(req, res) {
        res.status(200).json({
            status: "Register successful!"
        });
    }
);

app.post("/api/auth/done", (req, res) => {
    res.status(200).json({
        status: "Login successful!"
    });
});

/** LOCAL AUTH */

app.post("/api/auth/register", (req, res) => {
    console.log(req.body);
    Validation.validateUserReg({ 
        "username" : req.body.username,
        "password" : req.body.password,
        "email" : req.body.email }, (err) => {
        if (err) return res.status(400).json({
            status: "Not valid data!",
            err: err
        });
        process.nextTick(() => {
            User.findOne({$or:[{ "email" : req.body.email }, { "username" : req.body.username }]}, (err) => {
                if (err) return res.status(400).json({
                    status: "User already exists!"
                });
                User.register(new User({ username : req.body.username, email: req.body.email}), req.body.password, (err) => {
                    if (err) return res.status(400).json({
                        status: "Registration failed!"
                    });
                    passport.authenticate("local")(req, res, ()=> {
                        res.status(200).json({
                            status: "Register successful!"
                        });
                    });
                });
            });
        });
    });
});
  
/*app.post("/api/auth/login", passport.authenticate("localauth"), (req, res) => {
    res.redirect("/api/checkauth");
});*/
app.post("/api/auth/login", passport.authenticate("local"), function(req, res) {
    res.redirect("/api/checkauth");
});

app.get("/api/auth/login", function(req, res) {
    res.render("login", {user: req.user});
});

function isAuthenticated(req,res,next){
    if(req.user)
        return next();
    else
        return res.status(401).json({
            error: "User not authenticated"
        });
}

app.get("/api/logout", function(req, res){
    req.logout();
    res.redirect("/");
});

app.get("/api/checkauth", isAuthenticated, function(req, res){
    res.status(200).json({
        status: "Authorized!"
    });
});


app.get("/api/auth/notvalid", isAuthenticated, function(req, res){
    res.status(409).json({
        status: "User data is not valid!"
    });
    res.end();
});

/** FOODPLACE API */

app.get("/api/foodplace/:lat/:long", function(req, res){
    getPlaces(req.params.lat, req.params.long, null, function(err, response){
        if (err) return res.status(409).json({
            status: "Error with api!"
        });
        return res.status(200).json(response);
    });
});

app.get("/api/foodplace/:url", function(req, res){
    getPlaces(null, null, req.params.url, function(err, response){
        if (err) return res.status(409).json({
            status: "Error with api!"
        });
        return res.status(200).json(response);
    });
});

function getPlaces(lat, long, pagetoken, callback) {
    let requesturl;
    if (pagetoken && !lat && !long) {
        requesturl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=" + pagetoken +"&key=" + process.env.GOOGLE_MAPS_KEY;
    }
    if (lat && long && !pagetoken) {
        CoordValidation.validateCoord({ 
            "lat" : lat,
            "long" : long}, (err) => {
            if (err) return callback(err, null);
            requesturl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + long +"&radius=3000&type=restaurant&key=" + process.env.GOOGLE_MAPS_KEY;
        });
    }
    if (!pagetoken && (!lat || !long)) {
        return callback(null, null);
    }
    let info = Array();
    getJSON(requesturl, function(err, response){
        if (err) {
            return callback(err, null);
        }
        info.push({status : response.status});   
        if (response.status === "OK") {
            if(response.next_page_token) {
                info.push({ next_page_url : "http://" + process.env.HOST + ":" + process.env.WEB_PORT + "/api/foodplace/" + response.next_page_token});
            }
            let nearbyPlaces = Array();
            for(let i = 0; i < Object.keys(response.results).length; i++) {
                if(response.results[i].opening_hours && response.results[i].opening_hours.open_now == true) {
                    let placeInfo = {};
                    placeInfo.id = response.results[i].id;
                    placeInfo.name = response.results[i].name;
                    placeInfo.place_id = response.results[i].place_id;
                    placeInfo.rating = response.results[i].rating;
                    placeInfo.vicinity = response.results[i].vicinity;
                    placeInfo.refernamefoodplacesence = response.results[i].reference;
                    nearbyPlaces.push(placeInfo);
                }
            }
            info.push(nearbyPlaces);
        }
        return callback(null, info);
        
    });
}


app.post("/api/foodplace/:placeid/:placeFavorit", isAuthenticated, function(req, res){
    ReviewValidation.validateReview({ 
        "email" : req.user.email,
        "placeId" : req.params.placeid,
        "placeFavorit" : req.params.placeFavorit }, (err) => {
        if (err) return res.status(400).json({
            status: "Not valid data!",
            err: err
        });
        Review.findOne({"email" : req.user.email, "placeId": req.params.placeid}, (err, review) => {
            if (err) return res.status(500).json({
                status: "Failure!"
            });
            if (review) return res.status(400).json({
                status: "Review already exists!"
            });
            var newReview = new Review();
            newReview.email = req.user.email;
            newReview.placeId = req.params.placeid;
            newReview.placeFavorit = req.params.placeFavorit;
            newReview.save((err) => {
                if (err) throw err;
                return res.status(200).json({
                    status: "Review added!"
                });
            });
        });
    });
});

app.put("/api/foodplace/:placeid/:placeFavorit", isAuthenticated, function(req, res){
    ReviewValidation.validateReview({ 
        "email" : req.user.email,
        "placeId" : req.params.placeid,
        "placeFavorit" : req.params.placeFavorit }, (err) => {
        if (err) return res.status(400).json({
            status: "Not valid data!",
            err: err
        });
        let query = {"email" : req.user.email, "placeId": req.params.placeid};
        Review.findOneAndUpdate(query, { $set: { placeFavorit: req.params.placeFavorit }}, {upsert:true}, function(err){
            if (err) return res.send(500, { error: err });
            return res.status(200).json({
                status: "Review updated!"
            });
        });
    });
});

app.delete("/api/foodplace/:placeid", isAuthenticated, function(req, res){
    const query = {"email" : req.user.email, "placeId": req.params.placeid};
    Review.remove(query, function(err) {
        if (err) return res.status(500).json({
            status: "Failure!"
        });
        return res.status(200).json({
            status: "Review deleted!"
        });
    });
});





