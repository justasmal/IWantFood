const httpResText = function(status) {
    switch(status) {
    case 200:
        return "Request Successful";
    case 400:
        return "Bad Request";
    case 409:
        return "Conflict between request and server";
    case 503:
        return "There is error in servers, please wait";
    default:
        return "Unknown error";
    }
};


export default {httpResText};