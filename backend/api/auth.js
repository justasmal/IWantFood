import { OAuth2Strategy as GoogleStrategy } from "passport-google-oauth";
import User from "../models/user.models";
import Validation from "../validations/user.schema";
const passport = require("passport");

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_API_KEY,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/api/auth/google/callback"
},
function(accessToken, refreshToken, profile, done) {
    Validation.validateGoogleReg({ 
        "googleid" : profile.id,
        "googletoken" : accessToken,
        "name" : profile.displayName,
        "email" : profile.emails[0].value }, (err) => {
        if (err) return done(err);
        User.findOne({ "email" : profile.emails[0].value }, (err, user) => {
            if (err)
                return done(err, null);
            if (user) {
                User.findOne({ "googleid" : profile.id }, (err, userg) => {
                    if (err) return done(err, null);
                    if (userg) { 
                        return done(null, user);
                    } else {
                        user.googleid = profile.id;
                        user.googletoken = accessToken;
                        user.name  = profile.displayName;
                        user.save((err) => {
                            if (err) throw err;
                            return done(null, user);
                        });
                    }
                });
            } else {
                var newUser = new User();
                newUser.googleid    = profile.id;
                newUser.googletoken = accessToken;
                newUser.name  = profile.displayName;
                newUser.email = profile.emails[0].value;
                newUser.save((err) => {
                    if (err) throw err;
                    return done(null, newUser);
                });
            }
        });
    }
    );
}
));

passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


module.exports = passport;