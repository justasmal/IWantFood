import getJSON from "get-json";

let getFoodPlaces = function(lat, long){
    lat = -33.8585858;
    long = 151.2100415;
    getJSON("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat+ ","+ long +"&radius=1500&type=restaurant&key=" + process.env.GOOGLE_MAPS_KEY, function(error, response){
        return response;
    });
};

export default {getFoodPlaces};
