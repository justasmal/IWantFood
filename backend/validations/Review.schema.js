import Joi from "joi";

/*const foodPlace = object().keys({
    reviewId: number().min(1).required(),
    userId: number().min(1).required(),
    placeId: string().min(1).required(),
    placeFavorit: boolean().strict().required(),
    reviewDate: date().iso().required()
});

const testvalidresult = validate({
    userid: 1,
    reviewId: 1,
    placeId: "ElMxNjIsIExhbmUgTnVtYmVyIDcsIEJsb2NrIEgsIE5lYiBTYXJhaSwgU2FpbmlrIEZhcm0sIE5ldyBEZWxoaSwgRGVsaGkgMTEwMDYyLCBJbmRpYQ",
    placeFavorit: true
},
foodPlace, function (err, value) {
    if (!err) {
        console.log(err);
    } else {
        console.log("valid");
    }
    console.log(value);
});*/


const foodPlace = Joi.object().keys({
    email: Joi.string().email().required(),
    placeId: Joi.string().required(),
    placeFavorit: Joi.boolean().strict().required()


});


function validateReview(req, next) {
    Joi.validate(req, foodPlace, function (err) {
        if (err) {
            next(err);
        }
    });
    next(null);
}

export default { validateReview };