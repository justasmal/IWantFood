import Joi from "joi";

const coordinate = Joi.object().keys({
    lat: Joi.number().min(-90).max(90),
    long: Joi.number().min(-180).max(180)
});

function validateCoord(req, next) {
    Joi.validate(req, coordinate, function (err) {
        if (err) {
            next(err);
        }
    });
    next(null);
}
export default { validateCoord};


