import Joi from "joi";

/*const user = Joi.object().keys({
    userId: Joi.number().min(1).required(),
    username: Joi.string().alphanum().min(3).max(60).required(),
    password: Joi.string().max(256).required(),
    createDate: Joi.date().iso().required(),
    modifyDate: Joi.date().iso(),
    email: Joi.string().email().required(),
    accessToken: Joi.string().alphanum().min(3).max(200),
    coordinate: Joi.array().ordered([
        Joi.number().min(-90).max(90),
        Joi.number().min(-180).max(180)
    ])
});*/

const googleReg = Joi.object().keys({
    googleid: Joi.string().required(),
    googletoken: Joi.string().required(),
    name: Joi.string().required(),
    email: Joi.string().email().required()
});

const userReg = Joi.object().keys({
    username: Joi.string().alphanum().required(),
    password: Joi.string().required(),
    email: Joi.string().email().required()
});


function validateGoogleReg(req, next) {
    Joi.validate(req, googleReg, function (err) {
        if (err) {
            next(err);
        }
    });
    next(null);
}

function validateUserReg(req, next) {
    Joi.validate(req, userReg, function (err) {
        if (err) {
            next(err);
        }
    });
    next(null);
}





export default { validateUserReg, validateGoogleReg};


